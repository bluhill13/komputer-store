

const bankerButton = document.getElementById("loan-now-button");
const bankerBalance = document.getElementById("bank-balance");
let haveALoan = false;
let bankBalance;
//Work

function transferPayToBank(){
    setNewBalance(parseInt(document.getElementById("pay-amount").innerHTML.split(' ')[0]))
    document.getElementById("pay-amount").innerHTML = "0 NOK";
}

function earnMoneyByWorking(){
    let pay = document.getElementById("pay-amount");
    let temp = parseInt(pay.innerHTML.split(' ')[0]);
    temp += 100;
    pay.innerHTML = temp + " NOK";
}


//Banking 
function applyForLoan() {
    if (haveALoan == true){
        window.alert("You already have a loan! Buy a computer first"); 
    }else{
        document.getElementById("myModal").style.display = "block";
        let value = document.getElementById("bank-balance");
        let stringValue = value.innerHTML.split(' ');
        bankBalance = parseInt(stringValue[0]);
    }
}
function setNewBalance(loanAmount){
    let value = document.getElementById("bank-balance");
    let stringValue = value.innerHTML.split(' ');
    let temp = parseInt(stringValue[0]);
    temp += loanAmount;
    value.innerHTML = temp + " NOK";
}
function applyForAmount(){
    let loanAmount = parseInt(document.getElementById("loanAmount").value);
    if (loanAmount > bankBalance * 2){
        let errorMessage = document.getElementById("Perror");
        errorMessage.innerText = "Loan amount can not be greater than twice your balance!";
    }else{
        setNewBalance(loanAmount)
        document.getElementById("myModal").style.display = "none";
        //restrict several loans
        haveALoan = true;
    }
}

function spanOnClick() {
    document.getElementById("myModal").style.display = "none";
}

window.onclick = function(event) {
  if (event.target == document.getElementById("myModal")) {
    document.getElementById("myModal").style.display = "none";
  }
}



//Laptops
function buyLaptop(){
    let value = document.getElementById("bank-balance");
    let stringValue = value.innerHTML.split(' ');
    let balance = parseInt(stringValue[0]);

    let priceElement = document.getElementById("price");
    let stringprice = priceElement.innerHTML.split(" ");
    let price = parseInt(stringprice[0]);
    console.log(price);
    console.log(balance);
    if (balance >= price){
        let newBalance = price * -1;
        setNewBalance(newBalance);
        window.alert("You now own a laptop!");
    }else{
        window.alert("You dont have enough money!");
    }

}

function Laptop(name, price, info, imagePath){
    let Name = name;
    let Price = price;
    let Info = info;
    let ImagePath = imagePath;
    return {
        getName: function(){
            return Name;
        },
        getPrice: function(){
            return Price;
        },
        getInfo: function (){
            return Info;
        },
        getImagePath: function(){
            return ImagePath;
        }
    }
    }
let laptop1;
let laptop2;
let laptop3;
let laptop4;
(function() {
    laptop1 = Laptop("Dell XPS 13", "1300 NOK", "Sick performance in a 13 inc ultra book", "./assets/img/dell-xps-13.png")
    laptop2 = Laptop("Dell XPS 15", "1600 NOK", "Sick performance in a 15 inc ultra book, does also have a powerful GPU, and 4k screen for your videoediting", "./assets/img/dell-xps-15.png")
    laptop3 = Laptop("HP spectre x360", "1000 NOK", "Sick performance in a 13 inc ultra book, does also have a OK GPU and can be used as a tablet by twisting the screen backwards", "./assets/img/hp-spectre-x36.png")
    laptop4 = Laptop("Acer Ferrari 4000", "399 NOK", "Do you want a new heater in your home, that you also can use to surf the web? Then the ferrari 4000 pc is for you! Its almost as hot as their F1 car, impressively it scores 40 decibels higher in noise checks!", "./assets/img/acer-ferrari-4000.png");
})();

function selectLaptop(){
    let combobox = document.getElementById("laptops");
    let selectedItem = combobox.selectedIndex;
    //document.getElementById("demo").firstChild.innerHTML; = x.options[i].text;
    console.log(selectedItem);
    console.log(combobox.options[selectedItem].value);
    changeLaptopInfo(selectedItem);
}
function changeLaptopInfo(index){
    let laptopInfoTopBox = document.getElementById("info");
    let price = document.getElementById("price");
    let image = document.getElementById("laptopImage")
    var h = document.createElement("H1");
    var t = document.createTextNode("Your H1 text");
    let title = document.getElementById("laptops").options[index].text;
    document.getElementById("info").innerHTML = "";
    laptopInfoTopBox.appendChild(createH1Element(document.getElementById("laptops").options[index].text));
    switch(index){
        case 0:
            laptopInfoTopBox.appendChild(createPElement(laptop1.getInfo()));
            price.innerText = laptop1.getPrice();
            image.src = laptop1.getImagePath();
            break;
        case 1:
            laptopInfoTopBox.appendChild(createPElement(laptop2.getInfo()));
            price.innerText = laptop2.getPrice();
            image.src = laptop2.getImagePath();
            break;
        case 2:
            laptopInfoTopBox.appendChild(createPElement(laptop3.getInfo()));
            price.innerText = laptop3.getPrice();
            image.src = laptop3.getImagePath();
            break;
        case 3:
            laptopInfoTopBox.appendChild(createPElement(laptop4.getInfo()));
            price.innerText = laptop4.getPrice();
            image.src = laptop4.getImagePath();
            break;
        default:
            break;
    }
}



function createH1Element(text) 
{
    var h = document.createElement("H1");
    var t = document.createTextNode(text); 
    h.appendChild(t); 
    //document.body.appendChild(h);
    return h;
} 
function createPElement(text) 
{
    var p = document.createElement("P");
    var t = document.createTextNode(text); 
    p.appendChild(t); 
    //document.body.appendChild(h);
    return p;
} 

